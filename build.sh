#!/bin/sh

set -e;

package_version=`grep -P '^version = "[0-9.]+"$' Cargo.toml | grep -Po "[0-9.]+"`;

if [ -d pascal ]; then
  rm -r pascal;
fi

cargo test;
cargo build --release;

cp -R package pascal;
mkdir -p pascal/usr/bin;
cp target/release/pascal pascal/usr/bin;

chmod 755 pascal/DEBIAN/*inst pascal/DEBIAN/*rm;

sed -i "s/<package-version>/$package_version/g" pascal/DEBIAN/control;

dpkg-deb --build pascal;
