use std::collections::HashMap;
use std::error::Error;

use discord::model::{
    ChannelId, CurrentUser, Event, MessageId, Reaction, ReactionEmoji, ReadyEvent, RoleId, Server,
};
use discord::{Connection, Discord, GetMessages};
use log::{debug, error, info};

use crate::config::Configuration;

type Result<T> = std::result::Result<T, Box<dyn Error>>;

pub fn connect_start(config: Configuration) -> Result<()> {
    let (discord, mut connection, ready_event) = connect(&config)?;

    info!("Setting up...");

    debug!("Retrieving the Discord server...");
    let server = get_server(&discord)?;

    debug!("Creating missing roles...");
    let roles = get_or_create_roles(&discord, &config, &server)?;

    debug!("Finding the bot message...");
    let message = get_or_create_message(&discord, &config, &ready_event.user, &roles)?;

    info!("Bot started.");

    loop {
        match connection.recv_event() {
            Ok(Event::ReactionAdd(reaction)) => {
                if let Err(e) = handle_reaction_add(
                    &discord,
                    &server,
                    &ready_event.user,
                    ChannelId(config.channel_id),
                    message,
                    &roles,
                    reaction,
                ) {
                    error!("Unable to handle reaction: {}", e);
                }
            }
            Ok(_) => {}
            Err(err) => return Err(err.into()),
        }
    }
}

fn connect(config: &Configuration) -> Result<(Discord, Connection, ReadyEvent)> {
    let discord = Discord::from_bot_token(&config.token)?;
    let (connection, ready_event) = discord.connect()?;

    Ok((discord, connection, ready_event))
}

fn get_server(discord: &Discord) -> Result<Server> {
    let mut servers = discord.get_servers()?;

    if servers.len() == 1 {
        Ok(discord.get_server(servers.pop().unwrap().id)?)
    } else {
        Err(discord::Error::Other("No server or multiple servers were found!").into())
    }
}

fn get_or_create_roles(
    discord: &Discord,
    config: &Configuration,
    server: &Server,
) -> Result<Vec<(ReactionEmoji, RoleId)>> {
    let mut result = Vec::new();

    let roles = discord
        .get_roles(server.id)?
        .drain(..)
        .map(|r| (r.name, r.id))
        .collect::<HashMap<_, _>>();

    for (configured_role, emoji) in &config.roles {
        let role_id = match roles.get(configured_role) {
            Some(r) => *r,
            None => {
                discord
                    .create_role(
                        server.id,
                        Some(configured_role),
                        None,
                        None,
                        None,
                        Some(false),
                    )?
                    .id
            }
        };

        let emoji = server
            .emojis
            .iter()
            .find(|e| e.name == *emoji)
            .ok_or(discord::Error::Other("Emoji does not exists"))?;

        result.push((
            ReactionEmoji::Custom {
                name: emoji.name.clone(),
                id: emoji.id,
            },
            role_id,
        ));
    }

    Ok(result)
}

fn get_or_create_message(
    discord: &Discord,
    config: &Configuration,
    user: &CurrentUser,
    emojis: &[(ReactionEmoji, RoleId)],
) -> Result<MessageId> {
    let channel_id = ChannelId(config.channel_id);
    let mut messages_to_delete = Vec::new();
    let mut messages = discord.get_messages(channel_id, GetMessages::MostRecent, None)?;
    let mut bot_message = None;

    loop {
        for message in &messages {
            if message.author.id == user.id && bot_message.is_none() {
                bot_message = Some(message.clone());
            } else {
                messages_to_delete.push(message.id);
            }
        }

        if let Some(last_message) = messages.last() {
            messages =
                discord.get_messages(channel_id, GetMessages::Before(last_message.id), None)?;
        } else {
            break;
        }
    }

    for message_id in messages_to_delete {
        discord.delete_message(channel_id, message_id)?;
    }

    let bot_message = match bot_message {
        Some(m) => {
            if !m.content.is_empty() {
                discord.edit_message(channel_id, m.id, "")?;
            }

            m
        }
        None => discord.send_message(channel_id, &config.message_content, "", false)?,
    };

    discord.edit_embed(channel_id, bot_message.id, |b| {
        b.title(&config.message_title)
            .description(&config.message_content)
    })?;

    for reaction in bot_message.reactions {
        discord.delete_reaction(channel_id, bot_message.id, None, reaction.emoji)?;
    }

    for (emoji, _) in emojis {
        discord.add_reaction(channel_id, bot_message.id, emoji.clone())?;
    }

    Ok(bot_message.id)
}

fn handle_reaction_add(
    discord: &Discord,
    server: &Server,
    current_user: &CurrentUser,
    channel_id: ChannelId,
    bot_message_id: MessageId,
    emojis: &[(ReactionEmoji, RoleId)],
    reaction: Reaction,
) -> Result<()> {
    if reaction.user_id == current_user.id {
        return Ok(());
    }

    if reaction.channel_id != channel_id {
        return Ok(());
    }

    if reaction.message_id != bot_message_id {
        return Ok(());
    }

    discord.delete_reaction(
        channel_id,
        bot_message_id,
        Some(reaction.user_id),
        reaction.emoji.clone(),
    )?;

    let role = match emojis.iter().find(|(e, _)| *e == reaction.emoji) {
        Some((_, role_id)) => *role_id,
        None => return Ok(()),
    };

    let member = discord.get_member(server.id, reaction.user_id)?;

    if member.roles.contains(&role) {
        discord.remove_member_role(server.id, reaction.user_id, role)?;
    } else {
        discord.add_member_role(server.id, reaction.user_id, role)?;
    }

    Ok(())
}
