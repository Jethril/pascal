use std::collections::HashMap;
use std::env;
use std::fs::File;
use std::io::Read;
use std::path::{Path, PathBuf};

use serde::Deserialize;
use validator::Validate;

const CONFIG_FILE_NAME: &str = "pascal.toml";

custom_error! { pub Error
    IoError { source: std::io::Error } = "Could not read file: {source}",
    ParseError { source: toml::de::Error }  = "Could not parse file: {source}",
}

#[derive(Debug, Eq, PartialEq, Deserialize, Validate)]
pub struct Configuration {
    #[validate(length(equal = 59))]
    pub token: String,

    #[validate(length(min = 1))]
    pub message_title: String,

    #[validate(length(min = 1))]
    pub message_content: String,

    pub channel_id: u64,

    pub roles: HashMap<String, String>,
}

impl Configuration {
    pub fn load_from_file<P: AsRef<Path>>(path: P) -> Result<Self, Error> {
        let mut file = File::open(path)?;
        let mut str = String::new();

        file.read_to_string(&mut str)?;

        Ok(toml::from_str(&str)?)
    }
}

pub fn get_search_paths() -> Vec<PathBuf> {
    let mut result = Vec::new();

    if let Ok(path) = env::current_dir() {
        result.push(path.join(CONFIG_FILE_NAME));
    }

    if let Ok(path) = env::current_exe() {
        result.push(
            path.parent()
                .expect("Current executable should be in a folder")
                .join(CONFIG_FILE_NAME),
        );
    }

    result.push(("/etc/pascal/".to_owned() + CONFIG_FILE_NAME).into());

    result
}
