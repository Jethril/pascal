#[macro_use]
extern crate custom_error;

use std::{env, process};

use env_logger;
use log::{error, info};
use validator::Validate;

use config::Configuration;

mod bot;
mod config;

fn main() {
    env_logger::init();

    info!("Pascal bot v{} starting...", env!("CARGO_PKG_VERSION"));
    start_bot(load_configuration());
}

fn load_configuration() -> Configuration {
    info!("Locating configuration...");
    let path = config::get_search_paths().into_iter().find(|p| p.is_file());

    let path = match path {
        Some(p) => p,
        None => {
            error!("Could not find the configuration file!");
            process::exit(1);
        }
    };

    let path_str = path.to_str().unwrap_or("<not printable>");

    info!("Loading configuration from path '{}'...", path_str);
    let config = match Configuration::load_from_file(&path) {
        Ok(c) => c,
        Err(e) => {
            error!("Unable to load configuration file '{}': {}", path_str, e);
            process::exit(1);
        }
    };

    if let Err(validation_errors) = config.validate() {
        error!("The configuration is invalid: {}", validation_errors);
        process::exit(1);
    }

    config
}

fn start_bot(config: Configuration) -> ! {
    info!("Connecting to the Discord server...");

    if let Err(e) = bot::connect_start(config) {
        error!("A critical error occurred while running the bot: {}", e);
        process::exit(10);
    }

    unreachable!();
}
